#! /usr/bin/python3
# -*- coding: utf-8 -*-

"""
memusg.py calculates the execution time and memory peak of a program (and its arguments) given in argument.
It returns the execution time and the memory peak of the program.
For the program to measure the memory consumption of a program, its execution time must be greater than 500 ms.
The accuracy of the execution time is +/- 50 ms depending on your CPU.
Use -h or --help for more details.

author  : Emmanuel Clostres
python  : v3.8
version : 23/06/2021
"""

import os
import psutil
import subprocess
import sys
import time
from threading import Thread


THREAD_MEM = True
PROG_MEM_PEAK = 0
COM_MEM_PEAK = 0


def usage():
    """ Display the usage of the program in the terminal. """
    print("\nUsage :\n\n"
          "python memusg.py [path to the program] [program arguments]\n"
          "or\n"
          "python memusg.py [arguments]\n"
          "\n"
          "Arguments :\n"
          "  -h, --help                  how to use the program\n")


def print_error(msg: str):
    """
    Write a yellow error message in the terminal.
    :param msg: Message to display
    """
    print(f"\033[93m[ERROR]\033[0m {msg}")


def print_program_msg(msg: str):
    """
    Colors the messages of the parent program.
    :param msg: Message to display
    """
    print(f"\033[91m[Tester]\033[0m {msg}")


def join_arguments(list_of_arguments: list) -> str:
    """
    Returns a string with all arguments separated by a space, from a list of arguments.
    :param list_of_arguments: List of arguments
    :return: Arguments written as a string
    """
    arguments_str = ""
    for argument in list_of_arguments:
        arguments_str += f" {argument}"
    return arguments_str


def get_mem(step=1):
    """
    Gets and stores the biggest amount of used memory ('Resident Set Size' (rss) in kio converted to kB) at a constant
    time interval (default : 1 sec).
    :param step: Time interval in seconds to measure the memory used
    """
    global THREAD_MEM
    global PROG_MEM_PEAK
    global COM_MEM_PEAK
    process = psutil.Process(PROCESS.pid)
    while THREAD_MEM:
        time.sleep(step)
        try:
            prog_used_mem = process.memory_info().rss // 1024  # in kB
            com_used_mem = psutil.virtual_memory().used // 1024  # in kB
            if prog_used_mem > PROG_MEM_PEAK:
                PROG_MEM_PEAK = prog_used_mem
            if (com_used_mem - START_MEM) > COM_MEM_PEAK:
                COM_MEM_PEAK = (com_used_mem - START_MEM)
        except psutil.NoSuchProcess:
            # If the process ends before the thread
            THREAD_MEM = False
    print_program_msg("MEM thread stopped")


def time_formatting(sec: float) -> str:
    """
    Formats a time defined in seconds, in a format 'hour:min:sec'. Loss of the decimals information of the time if this
    one is higher than 60.
    :param sec: Time in seconds to format
    :return: Formatted time
    """
    int_sec = int(sec)
    if int_sec == 0:
        return f"{round(sec * 1000, 2)} ms"
    elif int_sec < 60:
        return f"{round(sec, 2)} s"
    elif int_sec < 3600:
        int_sec = int(round(sec, 0))
        mn = int_sec // 60
        int_sec %= 60
        return f"{mn} m {int_sec} s"
    else:
        int_sec = int(round(sec, 0))
        hr = int_sec // 3600
        int_sec %= 3600
        mn = int_sec // 60
        int_sec %= 60
        return f"{hr} h {mn} m {int_sec} s"


def mem_formatting(mem: int) -> str:
    """
    Formats a memory size (in kB), in kB, MB or GB depending on its value.
    :param mem: Memory in kB to format
    :return: Formatted memory size
    """
    if mem < 1_000:
        return f"{mem} kB"
    elif mem < 1_000_000:
        mem = round(mem / 1000, 2)
        return f"{mem} MB"
    else:
        mem = round(mem / 1_000_000, 2)
        return f"{mem} GB"


if __name__ == '__main__':
    # Get program arguments
    if len(sys.argv) == 2:
        if sys.argv[1][:1] == "-":
            if sys.argv[1] == "-h" or sys.argv[1] == "--help":
                usage()
                sys.exit(0)
            else:
                print_error(f"unknown argument '\033[92m{sys.argv[1]}\033[0m'")
                usage()
                sys.exit(2)
        else:
            program_path = sys.argv[1]
            program_params = ""
    elif len(sys.argv) > 2:
        program_path = sys.argv[1]
        program_params = join_arguments(sys.argv[2:])
    else:
        print_error("one or more mandatory arguments are missing !")
        usage()
        sys.exit(2)

    # Checks if the path exists
    if not os.path.exists(program_path):
        print_error(f"File '\033[92m{program_path}\033[0m' not found !")
        usage()
        sys.exit(2)

    # Program initialization
    print_program_msg("MEM thread initialization")
    START_MEM = psutil.virtual_memory().used // 1024
    print_program_msg(f"Memory used when the program started : {f'{str(round(START_MEM, 3))} kB'.ljust(len(str(round(START_MEM, 3))) + 4)}({mem_formatting(START_MEM)})")
    mem_thread = Thread(target=get_mem, args=(0.5,))
    start_time = time.time()

    # Launching the program
    command = f"python {program_path}{program_params}"
    print_program_msg(f"Command to execute   : '\033[92m{command}\033[0m'")
    print_program_msg("Launch of the subprocess")
    PROCESS = subprocess.Popen(command.split(" "))
    print_program_msg(f"Process ID (PID)     : {PROCESS.pid}")
    mem_thread.start()
    print_program_msg("MEM thread start")
    PROCESS.wait()

    # Exec time and pic memory calculation
    exec_time = time.time() - start_time
    THREAD_MEM = False
    mem_thread.join()

    # Display the result
    lj = max(len(str(round(exec_time, 3))), len(str(round(PROG_MEM_PEAK, 3))), len(str(round(COM_MEM_PEAK, 3)))) + 4
    print_program_msg(f"Execution time       : {f'{str(round(exec_time, 3))} s'.ljust(lj)}({time_formatting(exec_time)})")
    print_program_msg(f"Program memory peak  : {f'{str(round(PROG_MEM_PEAK, 3))} kB'.ljust(lj)}({mem_formatting(PROG_MEM_PEAK)})")
    print_program_msg(f"Computer memory peak : {f'{str(round(COM_MEM_PEAK, 3))} kB'.ljust(lj)}({mem_formatting(COM_MEM_PEAK)})")
