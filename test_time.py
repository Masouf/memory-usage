#! /usr/bin/python3
# -*- coding: utf-8 -*-

"""
The program sleeps for a time (in seconds) defined as an argument.

USAGE :
    python test_time.py <int>       duration for which the program will sleep

Result: in 3 seconds, the program returns about 3 +/- 0.05 seconds depending on your CPU.
"""

import time
import sys

# Default value
time_to_sleep = 3

if len(sys.argv) > 1 and len(sys.argv[1]) > 0:
    time_to_sleep = int(sys.argv[1])

print(f"Execution time test start for {time_to_sleep} sec !")

time.sleep(time_to_sleep)

