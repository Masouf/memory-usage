# Script performance tester

<pre>
Author  : Emmanuel Clostres  
Python  : v3.8  
Version : 23/06/2021  
</pre>

## Description
Python program that calculates the execution time and memory peak of another python program (with these arguments).

**memusg.py** is coded in `python 3.8`. It calculates the execution time and memory peak of a program 
(and its arguments) given in argument, and it returns the execution time and the memory peak of this program.

For the program to measure the memory consumption of a program, its execution time must be greater than 500 ms.
The accuracy of the execution time is +/- 50 ms depending on your CPU.

## New (June 23, 2021)
  - `memusg.py` now measures the memory peak of the program and the subprograms it depends on. This measurement is very much influenced by other programs that run at the same time as `memusg.py`, and depends on the memory used by the computer when `memusg.py` is launched.

## Example
   - `memusg.py test_mem.py 5`   
   - `memusg.py test_time.py 10`

## Citation

Emmanuel Clostres (2021)

## License
Free and unrestricted use.