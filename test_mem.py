#! /usr/bin/python3
# -*- coding: utf-8 -*-

"""
The program generates an ever-growing list, which consumes more and more memory over time.
Caution : beyond 12 sec the program can exceeds 5 GB of memory.

USAGE :
    python test_mem.py <int>           duration for which the memory will increase (do not exceed 10 seconds)

Result : in 10 seconds, the program can consumes about 1.3 GB (depending on your CPU).
"""


import time
import sys

# Default value
limit_time = 10

if len(sys.argv) > 1 and len(sys.argv[1]) > 0:
    limit_time = int(sys.argv[1])

print(f"MEM consumption test start for approximately {limit_time} sec !")

ll = ['foo', 'bar']

start_time = time.time()
while (time.time() - start_time) < limit_time:
    ll += ll[:(len(ll) // 2)]
    time.sleep(0.2)
